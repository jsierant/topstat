import statistics


def average(data):
    result = dict()
    for key, values in data.items():
        result[key] = sum(values[1]) / float(len(values[1]))
    return result


def standard_divation(data):
    result = dict()
    for key, values in data.items():
        result[key] = statistics.stdev(values[1])
    return result


def max_value(data):
    result = dict()
    for key, values in data.items():
        result[key] = max(values[1])
    return result
