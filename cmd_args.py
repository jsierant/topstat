import sys
import logging
import argparse

__variables = list()
__plots = list()
__positioned = None
__named_args = dict()

logger = logging.getLogger(__name__)
DESC = """\
A list of arguments are always seperated by coma.
"""


def __make_plot_action(parse_fun, plot_collection):
    class GatherPlotsAction(argparse.Action):
        def __init__(self, option_strings, dest, nargs=None, **kwargs):
            super(GatherPlotsAction, self).__init__(
                option_strings, dest, **kwargs)

        def __call__(self, parser, namespace, values, option_string=None):
            plot_collection.append((self.dest, parse_fun(values)))

    return GatherPlotsAction


__PARSER = argparse.ArgumentParser(
    description=DESC,
    formatter_class=argparse.RawTextHelpFormatter)


__LOG_LEVEL_STRINGS = ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']


def __log_level_string_to_int(log_level_string):
    if log_level_string not in __LOG_LEVEL_STRINGS:
        logger.critical('Unable to parse log level option argument "%s".',
                        log_level_string)
        logger.critical('See help for more information.')
        sys.exit(1)

    log_level_int = getattr(logging, log_level_string, logging.INFO)
    assert isinstance(log_level_int, int)
    logging.basicConfig(level=log_level_int)
    return log_level_int


def add_flag(name, short=None, help=None, default=False):  # pylint: disable=W0621,W0622
    global __variables
    action = 'store_true'
    if default:
        action = 'store_false'
    __variables.append(name.strip('-'))
    __PARSER.add_argument(
        short, name, default=default, action=action, help=help)


def add(name, short=None, default=None,
        parse=None, help=None):  # pylint: disable=W0621,W0622
    global __variables
    __variables.append(name.strip('-'))
    __PARSER.add_argument(
        short, name, nargs=1, default=[default], type=parse, help=help)


def add_plot(name, parse,             # pylint: disable=W0621
             short=None, help=None):  # pylint: disable=W0622
    __PARSER.add_argument(
        short, name, nargs=1,
        action=__make_plot_action(parse, __plots), help=help)


def add_positioned(name, help=None):  # pylint: disable=W0622
    global __variables
    global __positioned
    __variables.append(name)
    __PARSER.add_argument(name, nargs=1, help=help)
    __positioned = name


def parse():
    global __named_args
    parsed = __PARSER.parse_args()
    for opt in parsed.__dict__.keys():
        val = getattr(parsed, opt)
        if isinstance(val, bool):
            __named_args[opt] = val
        elif val and val[0] is not None and opt in __variables:
            __named_args[opt] = val[0]
    return __plots


def value(name):
    return __named_args[name]


def is_set(name):
    return name in __named_args


def plots():
    return __plots


add('--log_level', short='-l', parse=__log_level_string_to_int,
    help='select log level, {}'.format(__LOG_LEVEL_STRINGS))

