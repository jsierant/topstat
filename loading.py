import re
import collections
import logging
import sys
import datetime

import utils
import cmd_args

__logger = logging.getLogger(__name__)
__SIMPLE_TIME_RE = re.compile(r'top\s+-\s+(?P<time>\d\d:\d\d:\d\d).*')
__DEFAULT_FIRST_LINE_PATTERN = r'top\s+-'
__FMT = "%H:%M:%S"
__CTL_HEADER_RE = re.compile("start: (?P<time>\d\d:\d\d:\d\d), interval: (?P<interval>.+)")


def __extract_time(first_line):
    try:
        return __SIMPLE_TIME_RE.match(first_line).group('time')
    except AttributeError:
        return 'Unknown'


def __parse_period_arg(arg):
    start, end = arg.split('-')
    return Period(utils.parse_time(start),
                  utils.parse_time(end))


Period = collections.namedtuple('Period', ['start', 'end'])


def format_time(idx, period, start_time):
    return (start_time + datetime.timedelta(0, idx * period)).strftime(__FMT)


def parse_ctl_line(line):
    ctl_header = __CTL_HEADER_RE.match(line)
    return utils.parse_time(ctl_header.group('time')),\
        float(ctl_header.group('interval'))


def __check_value_available(value, name):
    if value is None and not cmd_args.is_set(name):
        __logger.critical('No "%s" in top file. See the Timing section from README.md for more info.', name)
        sys.exit(1)


def __add_top_info_header(snapshot, idx, start_time, interval):
    __check_value_available(start_time, 'start_time')
    __check_value_available(interval, 'interval')
    time = format_time(idx, interval or cmd_args.value('interval'),
                       start_time or cmd_args.value('start_time'))
    snapshot.insert(0, 'top - {}'.format(time))


def load_snapshots():
    global __logger
    first_line_re = re.compile(cmd_args.value('snapshot_start_pattern'))
    start_time = None
    interval = None
    first_line_ctl_line = False
    with open(cmd_args.value('input_file'), 'r') as file:
        snapshot = list()
        snapshots = list()
        first = True
        any_match = False
        idx = 0
        for line in file:
            line = line.strip()
            if not first_line_ctl_line and __CTL_HEADER_RE.match(line):
                start_time, interval = parse_ctl_line(line)
            first_line_ctl_line = True
            if first_line_re.match(line):
                __logger.debug('loading snapshot: %s', line)
                any_match = True
                if not first:
                    if not snapshot[0].startswith('top'):
                        __add_top_info_header(snapshot, idx, start_time, interval)
                    snapshots.append(snapshot)
                    snapshot = list()
                    idx = idx + 1

                first = False
            snapshot.append(line)
        if not snapshot[0].startswith('top'):
            __add_top_info_header(snapshot, idx, start_time, interval)
        snapshots.append(snapshot)
        if any_match:
            __logger.info('Extracted %d snapshots (%s-%s)',
                          len(snapshots),
                          __extract_time(snapshots[0][0]),
                          __extract_time(snapshots[-1][0]))
            return snapshots
        __logger.critical('No snapshots starting with pattern %s',
                          cmd_args.value('snapshot_start_pattern'))
        sys.exit()
        return []


def filter_snapshots_from_period(snapshots, period):
    filtered = list()
    for snap in snapshots:
        time = utils.parse_time(__extract_time(snap[0]))
        if period.start <= time <= period.end:
            filtered.append(snap)
    return filtered


def filter_snapshots(snapshots):
    global __logger
    filtered = None
    if cmd_args.is_set('period'):
        filtered = filter_snapshots_from_period(
            snapshots, cmd_args.value('period'))
        __logger.info('Filtered to %d snapshots (%s-%s)',
                      len(snapshots),
                      __extract_time(snapshots[0][0]),
                      __extract_time(snapshots[-1][0]))
    return filtered if filtered is not None else snapshots


cmd_args.add_positioned('input_file', help='top batch output file')
cmd_args.add('--period', short='-z', parse=__parse_period_arg,
             help='time period in form of HH:MM:SS-HH:MM:SS')
cmd_args.add('--snapshot_start_pattern', short='-a',
             default=__DEFAULT_FIRST_LINE_PATTERN,
             help='Snapshot first line pattern ({})'.format(__DEFAULT_FIRST_LINE_PATTERN))
cmd_args.add('--start_time', short='-b', parse=utils.parse_time,
             help='Measurement start time in form of HH:MM:SS')
