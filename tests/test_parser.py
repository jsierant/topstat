import parsing
import utils


def test_shall_parse_first_line():
    time = utils.parse_time('16:16:37')
    load = {'last_1m': 1.38, 'last_5m': 1.28, 'last_15m': 1.10}

    line = 'top - {} up  7:51,  1 user,  load average: {}, {}, {}'\
        .format(utils.format_time(time), load['last_1m'], load['last_5m'],
                load['last_15m'])

    parsed_time, parsed_load = parsing.__parse_first_line(line)
    assert time == parsed_time
    assert load == parsed_load


def test_shall_parse_cpu_stat():
    lines = ["%Cpu(s):  0.3 us,  0.0 sy,  0.0 ni, 99.3 id,  0.0 wa,  0.3 hi,"
             "  0.0 si,  0.0 st",
             "MiB Mem :  15924.6 total,  15307.3 free,    279.6 used,"
             "    337.7 buff/cache"]
    remining, info = parsing.__parse_cpu_stat(lines)
    assert info == {'us': 0.3, 'sy': 0.0, 'ni': 0.0, 'id': 99.3, 'wa': 0.0,
                    'hi': 0.3, 'si': 0.0, 'st': 0.0, 'unit': '%'}
    assert lines[1] == remining[0]


def test_shall_parse_cpu_cores_stat():
    lines = ["%Cpu0  :  0.3 us,  0.0 sy,  0.0 ni, 99.3 id,"
             "  0.0 wa,  0.3 hi,  0.0 si,  0.0 st",
             "%Cpu1  :  1.0 us,  0.7 sy,  0.0 ni, 98.3 id,"
             "  0.0 wa,  0.0 hi,  0.0 si,  0.0 st",
             "%Cpu2  :  0.3 us,  0.3 sy,  0.0 ni, 99.3 id,"
             "  0.0 wa,  0.0 hi,  0.0 si,  0.0 st",
             "MiB Mem :  15924.6 total,  15307.3 free,    "
             "279.6 used,    337.7 buff/cache"]
    _, info = parsing.__parse_cpu_stat(lines)
    assert isinstance(info, dict)
    assert info[0] == {'us': 0.3, 'sy': 0.0, 'ni': 0.0, 'id': 99.3, 'wa': 0.0,
                       'hi': 0.3, 'si': 0.0, 'st': 0.0, 'unit': '%'}
    assert info[1] == {'us': 1.0, 'sy': 0.7, 'ni': 0.0, 'id': 98.3, 'wa': 0.0,
                       'hi': 0.0, 'si': 0.0, 'st': 0.0, 'unit': '%'}
    assert info[2] == {'us': 0.3, 'sy': 0.3, 'ni': 0.0, 'id': 99.3, 'wa': 0.0,
                       'hi': 0.0, 'si': 0.0, 'st': 0.0, 'unit': '%'}


def test_shall_parse_mem_stat():
    line = 'MiB Mem :  15924.6 total,  15307.3 free,'\
        + '    279.6 used,    337.7 buff/cache'
    mem = parsing.__parse_mem_stat(line)
    assert mem == {'total': 15924.6, 'free': 15307.3, 'used': 279.6,
                   'buff/cache': 337.7, 'unit': 'MiB'}


EXPECTED_PROCESS_HEADER = ['pid', 'user', 'pr', 'ni', 'virt', 'res', 'shr',
                           's', '%cpu', '%mem', 'time+', 'command']


def test_shall_parse_processes_header():
    line = 'PID USER      PR  NI    VIRT    RES'\
        + '    SHR S  %CPU  %MEM     TIME+ COMMAND'
    header = parsing.__parse_process_header(line)
    assert header == EXPECTED_PROCESS_HEADER


def test_shall_parse_process_record():
    line = '526 jaro      20   0  253580  31996  14648 S'\
        + '   0.7   0.2   0:02.69 awesome'
    pid, pr = parsing.__parse_process_record(EXPECTED_PROCESS_HEADER, line)
    assert pid == 526
    assert pr['user'] == 'jaro'
    assert pr['pr'] == 20
    assert pr['ni'] == 0
    assert pr['virt'] == 253580
    assert pr['res'] == 31996
    assert pr['shr'] == 14648
    assert pr['s'] == 'S'
    assert pr['%cpu'] == 0.7
    assert pr['%mem'] == 0.2
    assert pr['time+'] == '0:02.69'
    assert pr['command'] == 'awesome'


def test_parse_snapshot():
    lines = """top - 21:26:05 up 6 min,  1 user,  load average: 0.1, 0.19, 0.09
Tasks: 135 total,   1 running, 134 sleeping,   0 stopped,   0 zombie
%Cpu0  :  0.3 us,  0.0 sy,  0.0 ni, 99.3 id,  0.0 wa,  0.3 hi,  0.0 si,  0.0 st
%Cpu1  :  1.0 us,  0.7 sy,  0.0 ni, 98.3 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu2  :  0.3 us,  0.3 sy,  0.0 ni, 99.3 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu3  :  0.0 us,  0.0 sy,  0.0 ni,100.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
MiB Mem :  15924.6 total,  15307.3 free,    279.6 used,    337.7 buff/cache
MiB Swap:      0.0 total,      0.0 free,      0.0 used.  15332.6 avail Mem

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
  526 jaro      20   0  253580  31996  14648 S   0.7   0.2   0:02.69 awesome
    8 root      -2   0       0      0      0 I   0.3   0.0   0:00.04 rcu_preemp
  522 jaro      20   0  218648  45136  27176 S   0.3   0.3   0:02.27 X
 1214 jaro      20   0   35328   3692   3308 R   0.3   0.0   0:00.02 top
    1 root      20   0  234472   8608   6704 S   0.0   0.1   0:00.73 systemd
    2 root      20   0       0      0      0 S   0.0   0.0   0:00.00 kthreadd

""".split('\n')

    top = parsing.parse_snapshot(lines)
    assert top.time == utils.parse_time('21:26:05')
    assert len(top.processes) == 6

