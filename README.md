**topstat.py** processes top batch output file and generates statistics.

#### Collecting data
There are three types of data which can be collected:
- processes (*top -b*),
- threads (*top -b -H*),
- CPU usage (overall or per core).

The last options can only be switched permanently in the top config (start top
and if cores are not visible press 1 (toggle option) and then W to write
changes).

##### ps
Data can also be collected by ps running following commands in loop:
- `date +"top - %X"; ps -A -o PID,PCPU,CMD` for processes
- `date +"top - %X"; ps -T -o PID,TID,PCPU,CMD,NAME` for threads

The script will remove processes when TID is included (entries for
which TID == PID) by default.

#### Timing
When a top output doesn't include time/load header entry (line starting 'top -'
is sometimes missing, e.g. on android devices) passing start time and interval
is required. It can be done via options (see help for more info) or required
information can be prepend to the output file in the form of
"start: HH:MM::SS, interval: %f", e.g. by command
`echo "start: $(date +%X), interval: ${INVERVAL}"` where the INTERVAL variable
contains top interval in seconds.
