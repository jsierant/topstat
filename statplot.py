import calc

__BAR_PLOT_CALCS = {'avg': {'calc': calc.average, 'title': 'Average'},
                    'stdiv': {'calc': calc.standard_divation,
                              'title': 'Standard deviation'},
                    'max': {'calc': calc.max_value,
                            'title': 'Max'}}


def plot(data, stats, plotter):
    for stat in stats:
        stats = __BAR_PLOT_CALCS[stat]['calc'](data)
        plotter.plot_bar(stats.keys(), stats.values(),
                         __BAR_PLOT_CALCS[stat]['title'])
