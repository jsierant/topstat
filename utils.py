import re
import datetime


__FLOAT_RE = re.compile(r"\d+(\.\d*)?$")
__INT_RE = re.compile(r"-?\d+$")


def to_number(val):
    if __INT_RE.match(val):
        return int(val), True
    if __FLOAT_RE.match(val):
        return float(val), True
    return val, False


def parse_time(time_str):
    return datetime.datetime.strptime(time_str, '%H:%M:%S')


def format_time(time):
    return datetime.datetime.strftime(time, '%H:%M:%S')


def format_time_line(times):
    return list(map(format_time, times))
