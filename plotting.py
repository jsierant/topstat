import sys
import os
import logging
import pathlib
import importlib
import collections
from pathlib import Path

import cmd_args


def __parse_img_size(val):
    return tuple(map(int, val.split('x')))


cmd_args.add('--tick_interval', short='-x', parse=int, default=10,
             help='x axis ticks interval in seconds')
cmd_args.add('--img', short='-i',
             help='create image instead of opening window')
cmd_args.add('--img_size', short='-y', default=(35, 20),
             parse=__parse_img_size,
             help='set image size in form of XXxYY in cm')
cmd_args.add('--footnote', short='-f', help='chart footnote')
cmd_args.add('--legend_location', short='-g', default='upper left',
             help="set legend location [best, 'upper right', 'upper left']")

logger = logging.getLogger(__name__)

try:
    importlib.import_module('cycler')
    importlib.import_module('matplotlib')
except ImportError:
    logger.critical('Unable to run the script due to missing dependences.')
    logger.critical('Please install cycler and matplotlib'
                    ' modules then try again.')
    sys.exit(1)


import cycler
import matplotlib

if os.environ.get('DISPLAY', '') == '':
    matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker


LINESTYLE_DASHDOTTED = (0, (3, 5, 1, 5, 1, 5))
COLORS = plt.cm.get_cmap('tab20').colors + plt.cm.get_cmap('tab20b').colors


class Plotter():
    def __init__(self, nrows):
        if os.environ.get('DISPLAY', '') == '' and not cmd_args.is_set('img'):
            logger.warning('Unable to find display.'
                           ' Interactive mode is not available.')
            logger.warning('An image generation can be used instead.'
                           ' See help for more info.')
            sys.exit(1)
        self.nrows = nrows
        self.ncols = 100
        self.chart_height = 10
        self.shape = (nrows * self.chart_height + self.nrows, self.ncols)
        self.row = 0
        self.ext_ratio = 0.15
        self.plt = plt
        self.first_plot = True
        self.logger = logging.getLogger(__name__)
        fig = plt.gcf()
        fig.set_size_inches(
            tuple(map(lambda x: int(x / 2.54),
                  cmd_args.value('img_size'))))
#          fig.canvas.set_window_title(
#              'topstat [{}]'.format(
#                  os.path.basename(cmd_args.value('input_file'))))

    def make_row_plotter(self, size):
        ext_colspan = int(self.ext_ratio * self.ncols)
        main_colspan = self.ncols - size * ext_colspan
        main_ax = plt.subplot2grid(
            self.shape, (self.row * self.chart_height, 0),
            colspan=main_colspan - 2, rowspan=self.chart_height - 1)
        axis = list()
        for idx in range(size):
            axis.append(plt.subplot2grid(
                self.shape, (self.row * self.chart_height,
                             main_colspan + idx * ext_colspan + 2),
                colspan=ext_colspan - 4, rowspan=self.chart_height - 1))
        self.row += 1
        first_row = self.first_plot
        self.first_plot = False
        return RowPlotter(main_ax, axis, self.row >= self.nrows, first_row)

    def show(self):
        self.__footnote()
        self.plt.show()

    def save(self, filename):
        self.__footnote()
        ext = pathlib.Path(filename).suffix.strip('.')
        if ext not in ['svg', 'png', 'pdf']:
            self.logger.critical('Unsupported file format \'%s\'', ext)
            sys.exit(1)
        self.plt.savefig(filename, format=ext, bbox_inches='tight')
        self.logger.info('Image generated: %s', filename)

    def __footnote(self):
        if cmd_args.is_set('footnote'):
            self.plt.figtext(0.9, 0.08, cmd_args.value('footnote'),
                             fontsize=8,
                             horizontalalignment='right')


class RowPlotter():
    def __init__(self, main_ax, ext_axs, add_xlabel, first_row):
        self.main_ax = main_ax
        self.ext_axs = ext_axs
        self.ext_axs_idx = -1
        self.add_xlabel = add_xlabel
        self.first_row = first_row

    def _get_next(self):
        self.ext_axs_idx += 1
        return self.ext_axs[self.ext_axs_idx]

    def _configure_main(self, xlabel, ylabel):
        self.main_ax.grid(
            color='grey', linestyle=LINESTYLE_DASHDOTTED, linewidth=0.7)

        if xlabel is not None and self.add_xlabel:
            self.main_ax.set_xlabel(xlabel)
        if ylabel is not None:
            self.main_ax.set_ylabel(ylabel)

        maxticks = 100000

        major_locator = mdates.SecondLocator(
            interval=cmd_args.value('tick_interval'))
        major_locator.MAXTICKS = maxticks
        self.main_ax.xaxis.set_major_locator(major_locator)
        self.main_ax.xaxis.set_major_formatter(
            mdates.DateFormatter(fmt='%H:%M:%S'))

        minor_locator = mdates.SecondLocator(
            interval=max(1, int(cmd_args.value('tick_interval') / 10)))
        minor_locator.MAXTICKS = maxticks
        self.main_ax.xaxis.set_minor_locator(minor_locator)

        for tick in self.main_ax.xaxis.get_major_ticks():
            tick.label.set_visible(self.add_xlabel)
            tick.label.set_fontsize(9)
            tick.label.set_rotation(40)
            tick.label.set_horizontalalignment('right')

    def plot_lines(self, data, ylabel, xlabel=None, title=None, yspace=None):
        if len(data.keys()) > 40:
            print('Unable to plot chart with more then 40 lines. Exiting.')
            sys.exit(1)

        if not yspace is None:
            self.main_ax.yaxis.set_major_locator(ticker.MultipleLocator(yspace))

        self.main_ax.set_prop_cycle(
            cycler.cycler('color', COLORS))

        plots = list()
        for label, plot_data in data.items():
            single_plot, = self.main_ax.plot(plot_data[0], plot_data[1],
                                             label=label)
            plots.append(single_plot)

        if len(plots) > 1:
            self.main_ax.legend(handles=plots, fontsize=9,
                                loc=cmd_args.value('legend_location'))
            self.main_ax.set_title(title or '')
        else:
            self.main_ax.set_title(' - '.join([title, list(data.keys())[0]]))
        self._configure_main(xlabel, ylabel)

    def plot_stack(self, data, ylabel, xlabel=None, title=None):
        if len(data.keys()) > 40:
            print('Unable to plot chart with more then 40 lines. Exiting.')
            sys.exit(1)

        self.main_ax.set_prop_cycle(
            cycler.cycler('color', COLORS))

        def fix_data():
            common_x = list()
            full_y = []
            def all_contains(x):
                for _, plot_data in data.items():
                    if not x in plot_data[0]:
                        return False
                return True

            for _, plot_data in data.items():
                for xx in plot_data[0]:
                    if all_contains(xx) and not xx in common_x:
                        common_x.append(xx)
            for labels, plot_data in data.items():
                y = []
                for idx, xx in enumerate(plot_data[0]):
                    if xx in common_x:
                        y.append(plot_data[1][idx])
                full_y.append(y)
            return common_x, full_y

        x, y = fix_data()
        plot = self.main_ax.stackplot(x, y, labels=list(data.keys()))

        self.main_ax.legend(handles=plot, fontsize=9)
        self.main_ax.set_title(title or '')
        self._configure_main(xlabel, ylabel)

    def plot_bar(self, xarray, yarray, title):
        axes = self._get_next()
        bars = axes.bar(xarray, yarray)
        axes.grid(color='grey', linestyle=LINESTYLE_DASHDOTTED, linewidth=0.7)
        axes.yaxis.grid(True)
        axes.xaxis.grid(False)

        axes.yaxis.set_minor_locator(ticker.AutoMinorLocator())

        if self.first_row:
            axes.set_title(title)
        cidx = 0
        for plot in bars:
            plot.set_color(COLORS[cidx])
            cidx += 1
        for tick in axes.xaxis.get_major_ticks():
            tick.label.set_visible(False)
        for tick in axes.yaxis.get_major_ticks():
            tick.label.set_fontsize(9)


def make_img_filename():
    if cmd_args.value('img') != '-':
        return cmd_args.value('img')
    (prefix, _, _) = cmd_args.value('input_file').rpartition('.')
    return prefix + '.png'


cmd_args.add('--kind', short='-k', default='lines',
             help='select main chart kind [\'lines\', \'stack\']')
