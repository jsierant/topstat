#!/usr/bin/env python3

import sys
import logging

import cpu
import proc
import parsing
import loading
import cmd_args
import plotting


cmd_args.add('--jobs', short='-j', default=1, parse=int,
             help='select number of jobs')
cmd_args.add('--title', short='-t', help='set chart title')
cmd_args.add(
    '--stats', short='-s', parse=lambda x: x.split(','), default=[],
    help='select statistics [\'avg\', \'stdiv\', \'max\']')
cmd_args.add('--mem_unit', short='-u', default='MiB', help='memory unit')


cmd_args.parse()


logger = logging.getLogger()

if not cmd_args.plots():
    logger.error('No chart specified (at least one is required).'
                 ' See help for more info.')
    sys.exit(1)


SNAPSHOTS = loading.filter_snapshots(
    loading.load_snapshots())

PARSED_SNAPSHOTS = parsing.parse(SNAPSHOTS)

PLOTTER = plotting.Plotter(len(cmd_args.plots()))


for name, data_filter in cmd_args.plots():
    if name == 'proc_chart':
        proc.plot(data_filter, PARSED_SNAPSHOTS,
                  PLOTTER.make_row_plotter(len(cmd_args.value('stats'))))
    if name == 'cpu_chart':
        cpu.plot(data_filter, PARSED_SNAPSHOTS,
                 PLOTTER.make_row_plotter(len(cmd_args.value('stats'))))
if cmd_args.is_set('img'):
    PLOTTER.save(plotting.make_img_filename())
    sys.exit(0)
PLOTTER.show()
