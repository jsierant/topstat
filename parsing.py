import re
import sys
import datetime
import logging
import collections
import multiprocessing
from typing import List

import utils
import cmd_args

__logger = logging.getLogger(__name__)

__FLOAT_RE = re.compile(r"-?\d+(\.-?\d*)?$")
__FLOAT_RE_START = re.compile(r"-?\d+(\.-?\d*)?[kmgtMGT]$")
__INT_RE = re.compile(r"-?\d+$")

__TIME_RE = r'(?P<time>\d\d:\d\d:\d\d)'
__LOAD_RE = r'(?P<last_1m>[\d\.]+),'\
    + r'\s(?P<last_5m>[\d\.]+),'\
    + r'\s(?P<last_15m>[\d\.]+)'
__FIRST_LINE_RE = re.compile(r'top - {}.*load average:\s{}'.format(
    __TIME_RE, __LOAD_RE))

__TOP_TIME_RE = re.compile(r'top - {}.*'.format(__TIME_RE))

__CPU_RE = re.compile(r'^(?P<unit>.*)Cpu.*:\s*(?P<params>.*)\s*')
__CPU_CORE_RE = re.compile(r'^(?P<unit>.*)Cpu(?P<idx>.*)\s*:\s*(?P<params>.*)\s*')
__CPU_PARAM_RE = re.compile(r'(?P<value>.+)\s(?P<name>.*)')

__CPU2_RE = re.compile(r'^(?P<total>\d+)%cpu\s*(?P<params>.*)\s*')
__CPU2_PARAM_RE = re.compile(r'(?P<value>.+)%(?P<name>.*)')

__MEM_RE = re.compile(r'^(?P<unit>.*)?\s*Mem.*:\s+(?P<params>.*)\s*')

__PROCESSES_START_RE = re.compile(r'\s*PID\s+.*')


def __to_float(val):
    try:
        return float(val)
    except ValueError:
        return 0.0


def __convert(val):
    if __INT_RE.match(val):
        return int(val)
    if __FLOAT_RE.match(val):
        return __to_float(val)
    if __FLOAT_RE_START.match(val):
        if val.endswith('k'):
            return __to_float(val[:-1]) * 1024
        if val.endswith('m'):
            return __to_float(val[:-1]) * 1024 * 1024
        if val.endswith('g'):
            return __to_float(val[:-1]) * 1024 * 1024 * 1024
        if val.endswith('t'):
            return __to_float(val[:-1]) * 1024 * 1024 * 1024 * 1024
        if val.endswith('K'):
            return __to_float(val[:-1]) * 1000
        if val.endswith('M'):
            return __to_float(val[:-1]) * 1000 * 1000
        if val.endswith('G'):
            return __to_float(val[:-1]) * 1000 * 1000 * 1000
        if val.endswith('T'):
            return __to_float(val[:-1]) * 1000 * 1000 * 1000 * 1000
    return val


def __parse_top_info_line(line):
    try:
        matched = __FIRST_LINE_RE.match(line)
        if matched:
            return (
                utils.parse_time(matched.group('time')),
                {'last_1m': float(matched.group('last_1m')),
                 'last_5m': float(matched.group('last_5m')),
                 'last_15m': float(matched.group('last_15m'))})
        matched = __TOP_TIME_RE.match(line)
        if matched:
            return utils.parse_time(matched.group('time')), None
    except AttributeError:
        pass
    return None, None


def __load_mem_stat(mem):
    params = mem.group('params').split(',')
    record = dict()
    for param in params:
        param = param.replace('+', ' ')
        value, name = param.split()
        record[name] = __convert(value)
    record['unit'] = mem.group('unit') or 'Byte'
    return record


def __split_cpu_prams(params):
    return params.split(',')


def __split_cpu2_prams(params):
    return params.split()


def __load_cpu_params(params, param_re):
    result = dict()
    for param in params:
        param_record = param_re.match(param)
        assert param_record is not None, param
        result[param_record.group('name')] = param_record.group('value')
    result['unit'] = '%'
    return result


def __parse_process_header(line):
    __logger.debug('parsing process header: %s', line)
    line = line.replace('[', ' ')
    line = line.replace(']', ' ')
    return [c.lower() for c in line.split()]


def get_process_name(process):
    if 'command' in process:
        return process['command']
    if 'args' in process:
        return process['args'].split()[0]
    if 'process' in process:
        return process['process']
    if 'name' in process:
        return process['name']
    if 'cmd' in process:
        return process['cmd']


def get_thread_name(process):
    if 'thread' in process:
        return process['thread']
    if 'cmd' in process:
        return process['cmd']


def get_name(process):
    pname = '[{}]{}'.format(process['pid'], get_process_name(process))
    tname = get_thread_name(process)
    if tname is not None and 'tid' in process:
        tname = '[{}]{}'.format(process['tid'], tname)
    assert pname is not None
    if pname == tname or tname is None:
        return pname
    return '{} {}'.format(pname, tname)


def __parse_process_record(header, line):
    __logger.debug('parsing process record: %s', line)
    process_record = {}
    line = re.sub(r'(\d+[kmgtKMGT])(\d+)', r'\1 \2', line)
    values = line.split()
    assert len(values) >= len(header), line
    for i, name in enumerate(header):
        if i == len(header) - 1:
            process_record[name] = ' '.join(values[i:])
        else:
            process_record[name] = __convert(values[i])
    return get_name(process_record), process_record


Snapshot = collections.namedtuple('Snapshot',
                                  ['time', 'load', 'cpu',
                                   'mem', 'processes'])
Snapshots = List[Snapshot]


def detect_interval(snapshots):
    if cmd_args.is_set('interval'):
        interval = cmd_args.value('interval')
        return datetime.timedelta(
            seconds=int(interval),
            microseconds=(interval - int(interval)) * 1000000)
    assert len(snapshots) > 1

    def _find_start_idx():
        start = snapshots[0].time
        if start is None:
            return None
        for i in range(1, len(snapshots)):
            delta = snapshots[i].time - start
            if delta >= datetime.timedelta(seconds=1):
                return i
        return None

    def _find_stop_idx(start_idx):
        start = snapshots[start_idx].time
        if start is None:
            return None
        for i in range(start_idx + 1, len(snapshots)):
            delta = snapshots[i].time - start
            if delta >= datetime.timedelta(seconds=1):
                return i
        return None

    def _print_error_and_finish():
        __logger.critical('Unable to detect snapshots interval.')
        __logger.critical('Specify interval using a command option.'
                          ' See help for more info.')
        sys.exit(1)

    start_idx = _find_start_idx()
    if not start_idx:
        _print_error_and_finish()
    stop_idx = _find_stop_idx(start_idx)
    if not stop_idx:
        _print_error_and_finish()
    distance = stop_idx - start_idx
    if distance == 1:
        return snapshots[stop_idx].time - snapshots[start_idx].time
    if 1000000 % distance != 0:
        __logger.critical('Invalid snapshots interval detected.')
        sys.exit(1)
    return datetime.timedelta(microseconds=int(1000000 / distance))


def fix_times(snapshots):
    interval = detect_interval(snapshots)

    def _update_time(idx):
        old = snapshots[idx]
        snapshots[idx] = Snapshot(snapshots[idx - 1].time + interval,
                                  old.load, old.cpu, old.mem, old.processes)

    if interval < datetime.timedelta(seconds=1):
        for i in range(1, len(snapshots)):
            _update_time(i)
    return snapshots


def split_header_processes(lines):
    idx = 0
    for line in lines:
        if __PROCESSES_START_RE.match(line):
            break
        idx = idx + 1
    return lines[0:idx], lines[idx:]


def parse_header(lines):
    cpu = dict()
    mem = None
    time = None
    load = None
    for line in lines:
        if line.startswith('top'):
            time, load = __parse_top_info_line(line)
        mcpu = __CPU_RE.match(line)
        if mcpu:
            cpu = __load_cpu_params(__split_cpu_prams(mcpu.group('params')),
                                    __CPU_PARAM_RE)

        mcpu = __CPU_CORE_RE.match(line)
        if mcpu:
            cpu[mcpu.group('idx')] =\
                __load_cpu_params(__split_cpu_prams(mcpu.group('params')),
                                  __CPU_PARAM_RE)

        mcpu = __CPU2_RE.match(line)
        if mcpu:
            cpu = __load_cpu_params(__split_cpu2_prams(mcpu.group('params')),
                                    __CPU2_PARAM_RE)
        mmem = __MEM_RE.match(line)
        if mmem:
            mem = __load_mem_stat(mmem)
    if time is None:
        __logger.critical("Failed to parse header, time entry is missing")
        sys.exit(1)
    return time, load, cpu, mem


def parse_snapshot(lines):
    header, process_lines = split_header_processes(lines)
    time, load, cpu, mem = parse_header(header)
    process_header = __parse_process_header(process_lines[0])
    processes = dict()
    for line in process_lines[1:]:
        if line:
            id, info = __parse_process_record(process_header, line)
            processes[id] = info
    return Snapshot(time, load, cpu, mem, processes)


def parse(snapshots):
    jobs = cmd_args.value('jobs')
    if jobs > 1:
        pool = multiprocessing.Pool(jobs)
        return fix_times(list(pool.map(parse_snapshot, snapshots)))
    return fix_times([parse_snapshot(s) for s in snapshots])


cmd_args.add('--interval', short='-d', parse=float,
             help='specify top snapshots interval if auto detection fails')
