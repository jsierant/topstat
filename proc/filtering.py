from proc.course import ProcessRecords
import cmd_args
import parsing

def filter_attrs(processes, attrs):
    result = list()
    for process in processes:
        values = dict()
        for attr in attrs:
            values[attr] = process.values[attr]
        result.append(
            ProcessRecords(process.id, process.times, values))
    return result


def filter_by_commands(processes, cmds):
    if not cmds: return processes
    return list(filter(lambda p: parsing.get_process_name(p.info) in cmds, processes))


def filter_by_command_pattern(processes, regex):
    if not regex: return processes
    return list(filter(lambda p: regex.match(p.id), processes))


def filter_threads(processes):
    if cmd_args.is_set('thrads_only') and cmd_args.value('thrads_only'):
        return list(filter(lambda p: 'tid' not in p.info or p.info['pid'] != p.info['tid'], processes))
    return processes


def filter_ntop(processes, ntop, attr):
    if not ntop: return processes
    avg_processes = [
        (p, sum(p.values[attr]) / float(len(p.values[attr])))
        for p in processes
    ]
    return [p[0] for p in sorted(avg_processes, key=lambda v: -v[1])[:ntop]]


def filter_proc(processes, data_filter):
    return filter_ntop(
        filter_by_command_pattern(
            filter_by_commands(filter_threads(processes), data_filter.cmd),
            data_filter.pattern),
        data_filter.ntop, data_filter.attr[0])
