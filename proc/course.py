import collections
import multiprocessing

import cmd_args

from parsing import Snapshots


ProcessRecords = collections.namedtuple(
    'ProcessRecords',
    ['id', 'info', 'times', 'values'])


def get_all_ids(snapshots: Snapshots):
    pids = set()
    for snap in snapshots:
        pids.update(snap.processes.keys())
    return pids


__ALL_VALUE_ATTRS = ['%cpu', '%mem', 'virt', 'res', 'shr', 'pr', 'ni']


def get_value_attrs(snapshots: Snapshots):
    for snapshot in snapshots:
        if snapshot.processes:
            values = list(snapshot.processes.values())[0]
            return \
                [attr for attr, _ in values.items()
                 if attr in __ALL_VALUE_ATTRS]
    return None


def make_info(process):
    result = dict()
    result['pid'] = process['pid']
    for key in ['name', 'command', 'cmd', 'thread', 'args', 'tid']:
        if key in process:
            result[key] = process[key]
    return result


def filter_short_processes(processes, size):
    return [p for p in processes if len(p.times) >= size]


class Extractor():
    def __init__(self, snapshots: Snapshots, attrs):
        self.snapshots = snapshots
        self.attrs = attrs

    def __call__(self, id):
        times = list()
        values = collections.defaultdict(list)
        process = None
        for snapshot in self.snapshots:
            if id in snapshot.processes:
                process = snapshot.processes[id]
                for attr in self.attrs:
                    values[attr].append(process[attr])
                times.append(snapshot.time)
        return ProcessRecords(id, make_info(process), times, values)


def extract(snapshots: Snapshots):
    attrs = get_value_attrs(snapshots)
    _extract = Extractor(snapshots, attrs)
    jobs = cmd_args.value('jobs')
    if jobs > 1:
        pool = multiprocessing.Pool(jobs)
        return filter_short_processes(
            pool.map(_extract, get_all_ids(snapshots)),
            cmd_args.value('min_meas'))
    return attrs, filter_short_processes(
        [_extract(p) for p in get_all_ids(snapshots)],
        cmd_args.value('min_meas'))


__DEFAULT_NMEAS = 5

cmd_args.add(
    '--min_meas', short='-n', parse=int, default=__DEFAULT_NMEAS,
    help='filter out processes with not enough data (default: {})'
         .format(__DEFAULT_NMEAS))
