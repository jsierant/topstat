import collections
import re
import logging
import sys

import cmd_args


Filter = collections.namedtuple(
    'Filter', ['ntop', 'pid', 'attr', 'cmd', 'pattern', 'orig'])
Filter.__new__.__defaults__ = (None,) * len(Filter._fields)


__HELP = """\
plot processes attributes defined by:
  - attr - attributes, mandatory
  - pid - PIDs
  - cmd - commands
  - pattern - command matching pattern
  - ntop - top n processes (on average) compared with
           the first argument of 'attr' option
e.g. cmd:awesome,kitty/attr:%%cpu will plot CPU usage
of awesome and kitty processes"""


__logger = logging.getLogger(__name__)


__handlers = {
    'attr': lambda v: v.split(','),
    'ntop': int,
    'cmd': lambda v: v.split(','),
    'pid': lambda v: [int(p) for p in v.split(',')],
    'pattern': lambda v: re.compile(v)
}


def format_filter(data_filter):
    return data_filter.orig


def __error(msg):
    __logger.critical('Failed to parse process chart filter.')
    __logger.critical(msg)
    __logger.critical('See help for more information about syntax.')
    sys.exit(1)


def __handle(params):
    try:
        values = dict()
        for name, val in params:
            if not name:
                __error('Missing initiated param definition.')
            handler = __handlers[name]
            if not val:
                __error('Missing value for argument \'{}\'.'
                        .format(name))
            values[name] = handler(val)
        if 'attr' not in values:
            __error('Missing mandatory argument \'attr\'.')
        return values
    except KeyError:
        __error('Unknown argument \'{}\'.'.format(name))
    except ValueError:
        __error('Unable to parse value \'{}\''
                ' of argument \'{}\'.'
                .format(val, name))
    return []


def __parse(arg):
    def __split(param):
        value_start = param.find(':')
        name = param[0:value_start]
        value = param[value_start + 1:]
        return [name, value]

    return Filter(**__handle(
        [tuple(__split(p)) for p in arg.split('/')]), orig=arg)


cmd_args.add_plot(
    '--proc_chart', short='-p', parse=__parse, help=__HELP)
cmd_args.add_flag(
    '--thrads_only', short='-e',
    help="Remove processes (entries where PID == TID)", default=True)
