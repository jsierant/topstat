import collections
import re
import sys
import logging

import statplot
from proc.options import *
from proc.course import *
from proc.filtering import *

__logger = logging.getLogger(__name__)

__courses = None
__attrs = None

__PROC_ATTR = {
    'virt': 'virtual memory',
    'res': 'residing memory',
    'shr': 'shared memory',
    '%cpu': 'CPU',
    '%mem': 'memory'}


def __attr_name(short):
    if short in __PROC_ATTR:
        return __PROC_ATTR[short]
    return 'attr:{}'.format(short)


def __unit(attr, mem_unit):
    if attr in ('%cpu', '%mem'):
        return '%'
    return mem_unit


def __make_title(data_filter):
    label = 'Process stats'
    if data_filter.pattern:
        label = '{} of "{}"'.format(label, data_filter.pattern.pattern)
    if data_filter.ntop:
        label = '{} - top {}'.format(label, data_filter.ntop)
    if len(data_filter.attr) == 1:
        label = '{} - {} usage'.format(label, __attr_name(data_filter.attr[0]))
    return label


def __scale(data, attr, mem_unit):
    if attr in ('%cpu', '%mem'):
        return data

    def get_scalar():
        if mem_unit == 'KiB': return 1
        if mem_unit == 'MiB': return 1024
        if mem_unit == 'GiB': return 1024 * 1024
    scalar = get_scalar()
    for _, plot_data in data.items():
        plot_data[1][:] = [x / scalar for x in plot_data[1]]
    return data


def plot(data_filter, snapshots, plotter):
    global __courses
    __logger.info('plotting')
    if not __courses: __attrs, __courses = extract(snapshots)
    for attr in data_filter.attr:
        if attr not in __attrs:
            __logger.critical('Unable to find process attribure'
                              ' "%s" in top snapshots.', attr)
            sys.exit(1)

    mem_unit = cmd_args.value('mem_unit')

    def _ylabel():
        return '[{}]'.format(
            __unit(data_filter.attr[0], mem_unit))

    def _label(cdata, attr, nattr):
        if nattr > 1:
            return '{}\\{}'.format(cdata.id, __attr_name(attr))
        return cdata.id

    def _plot():
        plot_data = dict()
        courses = filter_proc(__courses, data_filter)
        if not courses:
            __logger.critical('No data available for the given'
                              ' chart filter \'%s\'.',
                              format_filter(data_filter))
            sys.exit(1)

        for proc_data in courses:
            for attr in data_filter.attr:
                plot_data[_label(proc_data, attr, len(data_filter.attr))] =\
                    (proc_data.times,
                     proc_data.values[attr])
        plot_data = __scale(plot_data, data_filter.attr[0], mem_unit)
        if cmd_args.value('kind') == 'stack':
            plotter.plot_stack(plot_data, _ylabel(), xlabel='time',
                               title=__make_title(data_filter))
        else:
            plotter.plot_lines(plot_data, _ylabel(), xlabel='time',
                               title=__make_title(data_filter), yspace=5)

        return plot_data
    statplot.plot(_plot(), cmd_args.value('stats'), plotter)
