import sys
import collections

import cmd_args
import utils


Filter = collections.namedtuple('Filter', ['cores', 'attrs'])

__HELP = """\
plot CPU usage defined by:
  - attr - attributes, mandatory
  - core - number or range in form of start-end (default: all cores)
e.g. attr:id/core:1 will plot idle CPU attribure for core 1"""


def __parse(define):
    args = define.split('/')
    cores = None
    attr = None
    for arg in args:
        param, value = arg.split(':')
        if param == 'core':
            val_range = value.split('-')
            if len(val_range) == 2:
                cores = range(int(val_range[0]), int(val_range[1]))
                continue
            core, success = utils.to_number(value)
            if not success:
                print('Unable to parse argument for cpu plot: \'{}\''
                      .format(define))
                sys.exit(1)
            cores = [core]
        if param == 'attr':
            attr = value.split(',')
    return Filter(cores, attr)


cmd_args.add_plot(
    '--cpu_chart', short='-c', parse=__parse, help=__HELP)
