import sys
import collections

import utils
import parsing
import statplot
import cpu.options
import cmd_args


__YLABEL = '[%]'
__TITLE = 'CPU stats'
__XLABEL = 'time'

__CPU_ATTR = {
    'us': 'un-niced user processing',
    'sy': 'kernel processing',
    'ni': 'niced user processing',
    'id': 'idle handling',
    'wa': 'waiting for I/O completion',
    'hi': 'servicing hardware interrupts',
    'si': 'servicing software interrupts',
    'st': 'time stolen by hypervisor'}


def __attr_name(short):
    if short in __CPU_ATTR:
        return __CPU_ATTR[short]
    return 'attr:{}'.format(short)


def __make_label(core, attr):
    return '/'.join(['core:{}'.format(core), attr])


def __plot_multicore(attrs, times, courses, plotter):
    attrs = attrs or courses[0].keys()
    cores = courses.keys()
    data = dict()
    for core in cores:
        for attr in attrs:
            data[__make_label(core, __attr_name(attr))] =\
                (times, courses[core][attr])
    plotter.plot_lines(data, __YLABEL, __XLABEL, __TITLE)
    return data


def __plot_whole_cpu(attrs, times, courses, plotter):
    attrs = attrs or courses.keys()
    data = {__attr_name(a): (times, courses[a]) for a in attrs}
    plotter.plot_lines(data, __YLABEL, __XLABEL, __TITLE)
    return data


def select_cpu(snaps):
    result = dict()
    if parsing.multicore_data(snaps[0].cpu):
        for core in snaps[0].cpu:
            values = dict()
            for attr in snaps[0].cpu[core].keys():
                values[attr] = [snap.cpu[core][attr] for snap in snaps]
            result[core] = values
        return result
    return {a: [snap.cpu[a] for snap in snaps] for a in snaps[0].cpu.keys()}


def plot(data_filter, snapshots, plotter):
    times = list(map(lambda s: s.time, snapshots))
    courses = select_cpu(snapshots)
    stat_data = None
    if parsing.multicore_data(courses):
        stat_data = __plot_multicore(
            data_filter.attrs, times, courses, plotter)
    else:
        stat_data = __plot_whole_cpu(
            data_filter.attrs, times, courses, plotter)
    statplot.plot(stat_data, cmd_args.value('stats'), plotter)
